# Trabajo Final: Gestión de Proyectos y Prácticas Profesionalizantes II

#### Introducción:

El proyecto “Sector Ciencias Naturales” trata sobre reproducir un vídeo explicando la vida de animales que se encuentran en dicho sector con los componentes sensor ultrasónico y con placa Arduino Uno.


#### Descripción de dispositivos:

El propósito de dicho proyecto es que comience a reproducir un vídeo a través de un proyector dando información  sobre dichas especies que están en ese sector donde este se activara cuando una persona se aproxime a 40 cm de distancia.


#### Hardware involucrado: los equipos de hardware utilizados son:
- Sensor Ultrasónico HC-SR04
- Arduino Uno-Placa Base
- Laptop
- CPU
- Cables Jumpers Macho y Hembra 
- Cable USB
- Proyector 
- Monitor 
- Mouse 
- Teclado
- Cable (Alargue y Zapatilla)
- Cable HDMI
- Parlantes

#### Software involucrado:
- Arduino IDE
- Python 3
- Antix (Linux) 

#### Códigos:
##### Codigo de Python:
```
#! /usr/bin/Python

#Importamos la librería de Py Serial
import serial
import vlc
import time
import sys

#Abrimos el puerto del Arduino a 9600
PuertoSerie = serial. Serial ('/dev/ttyUSB0', 9600)
player = vlc. MediaPlayer("audio.mp3")
#Creamos un buble sin fin

while True:
  
#leemos hasta que encontramos el final de línea
  sArduino = PuertoSerie.readline()
#convertimos la variable de tipo bytes a str
  distancia = sArduino.decode()
#convertimos la variable de tipo str a float
  distanciaf = float(distancia)
  
#print (distanciaf)
# player = vlc.MediaPlayer("audio.mp3")
  
  if distanciaf < 40 :
    print ("estas a menos de 40")
    player.play()
    time.sleep(108) #segundos
    print ("fin")
    player.stop()
    #sArduino = PuertoSerie.readline()
    #PuertoSerie.reset_input_buffer()
    #PuertoSerie.reset_output_buffer()
    #SPuertoSerie = serial.Serial('/dev/ttyUSB0', 9600)
    #https://es.linux-console.net/?p=11681

```
##### Código de Arduino:
```
const int pinTrigger = 2;  //Variable que contiene el número del pin al cual conectamos la señal "trigger"
const int pinEcho = 3;   //Variable que contiene el número del pin al cual conectamos la señal "echo"
int led=13;
void setup() {
  Serial.begin(9600); //Configuramos la comunicación serial
  pinMode(pinTrigger, OUTPUT); //Configuramos el pin de "trigger" como salida
  pinMode(pinEcho, INPUT);  //Configuramos el pin de "echo" como entrada
  pinMode(led, OUTPUT);
  digitalWrite(pinTrigger, LOW); //Ponemos en voltaje bajo(0V) el pin de "trigger"
}

void loop()
{

  unsigned long t; //Variable de tipo unsigned long que contendrá el tiempo que le toma a la señal ir y regresar
  float d; //Variable de tipo float que contendrá la distancia en cm

  digitalWrite(pinTrigger, HIGH); //Ponemos en voltaje alto(5V) el pin de "trigger"
  delayMicroseconds(10); //Esperamos en esta línea para conseguir un pulso de 10us
  digitalWrite(pinTrigger, LOW); //Ponemos en voltaje bajo(0V) el pin de "trigger"
t = pulseIn(pinEcho, HIGH);         //Utilizamos la función  pulseIn() para medir el tiempo del pulso/echo
  d = t * 0.000001 * 34300.0 / 2.0; //Obtenemos la distancia considerando que la señal recorre dos veces la distancia a medir y que la velocidad del sonido es 343m/s
  
  //Serial.print("Distancia: ");
  Serial.print(d);
  //Serial.print("cm");
  Serial.println();
  if (d < 40){
  delay(6800);
  } else {delay(1000);
    }
  //Nos mantenemos en esta línea durante 100ms antes de terminar el loop
}

```
#### Descripción de su funcionamiento:

El sensor se enciende mediante la conexión de un cable USB hacia la computadora, además tiene configurado que cuando una persona se aproxime a 40 centímetros este empiece a reproducir el  vídeo y se vuelva a reproducir cada 2 minutos.


#### Conexiones entre dispositivos:

Las conexiones se establecen en la placa de Arduino Uno, es decir, para que esta se encienda debe conectarse a la computadora a través del cable USB. El sensor esta conectado mediante cables jumper (tanto hembra como macho) a la placa Arduino.


![](https://naylampmechatronics.com/img/cms/Blog/Tutorial%20arduino%20y%20HC-SR04/Tutorial%20sensor%20ultrasonico%202.jpg)

#### Requisitos para la instalación de Arduino:
- 1 GB de espacio libre en el disco duro.
- 1 GB de memoria RAM.
- Procesador Pentium 4 o superior.
- Sistema operativo antix (Linux).

#### Configuraciones:
##### Del Arduino:
- Primero configuramos los pines y la comunicación serial a 9800 baudios
- Ahora en el bucle void loop() empezamos enviando un pulso de 10us al Trigger del sensor
- Seguidamente recibimos el pulso de respuesta del sensor por el pin Echo, para medir el pulso usamos la función pulseIn(pin, value)  
- La variable t, tiene el tiempo que dura en llegar el eco del ultrasonido, el siguiente paso es calcular la distancia entre el sensor ultrasónico y el objeto.
- Finalmente enviamos serialmente el valor de la distancia y terminamos poniendo una pausa de 100ms, que es superior a los 60ms  recomendado por los datos técnicos del sensor.
- Conecte el Arduino Uno y cargue el programa.
- Después de esto el Arduino y sensor ya deben estar trabajando, para poder visualizar los datos vaya a herramientas y habrá el monitor serial.

##### De Python:

Primero importamos las librerías PySerial, vlc, time y sys.
Luego abrimos el puerto del Arduino al 9600.
Creamos un bucle sin fin con la funcion while True para que se lea hasta encontrar el final de la línea.
Convertimos la variable de tipo bytes primero a str y luego de str a float para que python pueda leer el audio.
Luego la funcion distanciaf donde se colocara 40, que son los cm a la que cuando se acerque la persona comience a proyectarse el vídeo.
Y por ultimo las funciones que permitiran las conexiones con el Arduino: puertoserial y serial.serial



##### Librerías utilizadas
- PySerial
- Vlc
- Time
- Sys

#### Procedimientos de instalación de software utilizados
##### Instalación o actualización de Python 3 para Linux: 
###### Paso 1: Comprueba si Python está preinstalado.

Abre una ventana de terminal y escribe el siguiente comando para comprobar si Python ya está instalado en tu sistema: python --version. Pulsa Intro, y si Python está instalado, verás que se muestra el número de versión. Si estás satisfecho con la versión instalada, puedes saltarte los pasos restantes. Si Python no está instalado o quieres instalar una versión más reciente, pasa al siguiente paso.


###### Paso 2: Instalar mediante el Gestor de paquetes.

La forma más sencilla de instalar Python en Linux es a través del Gestor de Paquetes específico de tu distribución. Aquí tienes un par de comandos populares:
Ubuntu/Debian: sudo apt-get install python3
Fedora: sudo dnf install python3
Ejecuta el comando adecuado para tu distribución e introduce tu contraseña cuando se te solicite. El Gestor de Paquetes descargará e instalará la última versión de Python 3 disponible en los repositorios.



###### Paso 3: Descarga la última versión de Python.

Ve al sitio web de Python y descarga la última versión de Python para Linux desde su sitio web. Extrae el contenido del tarball.


###### Paso 4: Compilar desde el código fuente (opcional).

Si prefieres compilar Python desde el código fuente, puedes hacerlo obteniendo primero el último código fuente de Python del repositorio Git oficial de Python.
Clonarás el código fuente desde GitHub con el siguiente código: git clone https://github.com/python/cpython. Desde ahí, navega hasta el directorio extraído utilizando cd cpython.


###### Paso 5: Configura el script y completa el proceso de compilación.

Tu siguiente paso es ejecutar el siguiente código:

```
./configure
make
make test
sudo make install
```

######  Paso 6: Verifica la instalación con el Terminal.

Después de instalar Python, puedes verificar la instalación abriendo una ventana de terminal y escribiendo el siguiente comando: python3 --version. Pulsa Intro, y deberías ver la versión de Python que has instalado. Ya has terminado con el proceso de instalación.


##### Instalación o actualización de Arduino IDE para Linux:

Descargar Arduino para Linux: Para descargar Arduino para Linux, primero tenes que averiguar que tipo de sistema tienes. Por ejemplo, puede ser de 32 o de 64 bits. Entonces para averiguar si tu Ubuntu es 32 o 64 bits puedes ejecutar el siguiente comando:        uname -m
Ya que sabes si tu sistema es de cuantos bits, entonces puedes descargar la versión adecuada. En los siguientes enlaces puedes descargarlo directamente.
- Arduino 64 bits.
- Arduino 32 bits.
Descomprimir el archivo descargado: Para descomprimir el archivo, basta con seleccionar con el clic derecho la opción: 
«Descomprimir aqui» o en ingles: «Extract Here».
Instalar Arduino en Linux mediante comandos: Para Instalar Arduino en Linux, primero se requiere de abrir una terminal. Entonces para abrir la terminal, falta con buscarla en el menú de programas y ejecutarla. Una vez que ya esta abierta, lo primero que tenemos que hacer es cambiar el cursor a la carpeta donde se descomprimió el Arduino. Para después cambiar los permisos de ejecución del instalador y finalmente ejecutar el instalador.


![](https://hetpro-store.com/TUTORIALES/wp-content/uploads/2019/03/Instalar-Arduino-en-Ubuntu.jpg)

En mi caso, este proceso me arrojo varios errores como se muestran en la imagen. Pero por el contrario, estos no provocaron algún comportamiento anormal en la instalación. Este proceso generará un icono de acceso directo en el escritorio. Al inicio se verá como raro, ya que no contiene el icono de Arduino. Como aparece en la siguiente imagen:


![](https://hetpro-store.com/TUTORIALES/wp-content/uploads/2019/03/Icono-ide-Arduino.jpg)

Esto es porque no le hemos otorgado permiso para poder confiar y ejecutar dicho programa. Entonces basta con darle doble clic al programa y darle el permiso. Como se muestra en la siguiente imagen:


![](https://hetpro-store.com/TUTORIALES/wp-content/uploads/2019/03/Permisos-IDE-Arduino.jpg)

Comando para asignar permisos al puerto serial Arduino en Linux: En algunos casos, el uso del puerto serial esta restringido. Esto es que no cualquier usuario en Linux (Ubuntu 18.04) puede hacer uso del puerto serial. Entonces en el caso de ejecutar el IDE de Arduino en Linux, nos podemos encontrar con el problema de no tener permiso de usar el puerto serial en Linux. Para esto resolver este problema se puede ejecutar el siguiente comando: sudo usermod -a -G dialout nombreUsuario
Donde le estamos diciendo que al nombreUsuario se le asigne el uso del puerto serial (dialout).
Obtener el nombre de usuario en Linux: Para obtener el nombre de usuario en Linux, puedes ejecutar el siguiente comando. Este comando te regresara dicho dato: whoami
Una vez que hemos cambiado los permisos del puerto serial, necesitamos cerrar la sesión y volver a logearnos y ya estará solucionado el problema. También puedes reiniciar Ubuntu y será el mismo efecto.



##### Implementación del dispositivo:

Primero se realizara la colocación del proyector en el lugar necesario y la conexion del cable hml en la computadora con la ayuda de un alargue, para abarcar mayor distancia, en un lugar al lado de la pared para que no sea tocado. Luego se colocara el sensor con el arduino en un lugar poco visible a los visitantes y que al mismo tiempo permita que su acercamiento active el vídeo.
Se realizaran las conexiones de los cables en el arduino uno. Luego el cable usb se conectara con el arduino en la computadora. Se guardaran y ejecuratan los programas realizados: el arduino ide y el de python para probar su funcionamiento.



###### Otras Observaciones:
###### Fuentes de Información:
https://naylampmechatronics.com/blog/10_tutorial-de-arduino-y-sensor-ultrasonico-hc-sr04.html

https://hetpro-store.com/TUTORIALES/instalar-arduino-1-8-9-en-ubuntu-18-04/

##### Autores:
- Gonzalez, Natalia 
- Videla, Gabriel 
- Rodríguez, Abril
